//
//  AppDelegate.h
//  XLBasicConfigurationDemo
//
//  Created by admin on 2019/3/27.
//  Copyright © 2019 薛磊. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

